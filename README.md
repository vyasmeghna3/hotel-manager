This is the first ever project that I made, as a part of my coursework in Computer Science as a high school student back in 2015. This is a console-based Hotel Managing application, only intended for practice purpose. 
The project is built in C++, using TURBO C++.<br>
The concepts implemented are :<br>
    * Graphic programming<br>
    * Object-oriented programming<br>
    * File Handling using binary files.<br>

